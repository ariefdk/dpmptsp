package com.example.dpmptsp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.Fragment.HistoryFragment;
import com.example.dpmptsp.Fragment.IzinPuskesmasFragment;
import com.example.dpmptsp.Model.DataDoc;
import com.example.dpmptsp.Model.Dokumen;
import com.example.dpmptsp.Util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import static com.example.dpmptsp.Util.Util.GET_PROSES;
import static com.example.dpmptsp.Util.Util.NAMA;
import static com.example.dpmptsp.Util.Util.PRINT;

public class DokumenActivity extends AppCompatActivity {

    private ArrayList<Dokumen> list;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private WebView webView;

    private TextView textViewBelumAdaDokumen;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dokumen);

        textViewBelumAdaDokumen = (TextView) findViewById(R.id.tv_blm_ada_dokumen);
        progressBar = (ProgressBar) findViewById(R.id.pb_dokumen);

        webView = (WebView) findViewById(R.id.wv_print);
        webView.getSettings().setJavaScriptEnabled(true);

        list = new ArrayList<>();
        addData();

        recyclerView = (RecyclerView) findViewById(R.id.rv_dokumen);
    }

    public void addData() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_PROSES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("proses");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String namaIzin = jsonArray.getJSONObject(i).getString("nama_izin");
                                    String namaPemohon = jsonArray.getJSONObject(i).getString("nama_pemohon");
                                    String namaDokumen = namaIzin + " - " + namaPemohon.toUpperCase();
                                    String status = jsonArray.getJSONObject(i).getString("status");
                                    int idProses = jsonArray.getJSONObject(i).getInt("id_proses");
                                    if (status.equals("selesai")) {
                                        list.add(new Dokumen(namaDokumen, idProses, i));
                                    }
                                }
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                adapter = new rv_adapter(getApplicationContext(), list);
                                recyclerView.setAdapter(adapter);
                                progressBar.setVisibility(View.GONE);
                            } else {
                                textViewBelumAdaDokumen.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (
                                JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DokumenActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("pemohon", Util.ID);
                return params;
            }
        };

        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }


    private void addList(String nama, int id, int position) {
        list.add(new Dokumen(nama, id, position));
    }

    class rv_adapter extends RecyclerView.Adapter<rv_adapter.rv_holder> implements View.OnClickListener {

        LayoutInflater inflater;
        ArrayList<Dokumen> dokumen;

        public rv_adapter(Context context, ArrayList<Dokumen> dokumen) {
            this.inflater = LayoutInflater.from(context);
            this.dokumen = dokumen;
        }

        @NonNull
        @Override
        public rv_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.rv_row_dokumen, parent, false);
            return new rv_holder(itemView, this);
        }

        @Override
        public void onBindViewHolder(@NonNull rv_holder holder, int position) {
            Dokumen current = dokumen.get(position);
            holder.namaDokumen.setText(current.getNama_dokumen());
            holder.id = current.getId_proses();
            holder.position = current.getPosition();
        }

        @Override
        public int getItemCount() {
            return dokumen.size();
        }

        @Override
        public void onClick(View v) {
        }

        class rv_holder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private RecyclerView.Adapter adapter;
            private TextView namaDokumen;

            private int id;
            private int position;

            public rv_holder(View itemView, rv_adapter adapter) {
                super(itemView);
                namaDokumen = (TextView) itemView.findViewById(R.id.tv_nama_dokumen_pdf);
                itemView.setOnClickListener(this);

                this.adapter = adapter;
            }

            @Override
            public void onClick(View v) {
                if (v == itemView) {
                    String url = PRINT + id + ".pdf";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                }
            }
        }
    }
}
