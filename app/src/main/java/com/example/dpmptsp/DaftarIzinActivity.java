package com.example.dpmptsp;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dpmptsp.Util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DaftarIzinActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {

    private EditText editText_nama;
    private EditText editText_ktp;
    private Button button_daftar;
    private TextView textView_link_syarat;

    private Spinner spinner_jenis_perizinan;

    private CheckBox checkBox_persyaratan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_izin);

        final AlertDialog.Builder alert_ktp = new AlertDialog.Builder(this);
        alert_ktp.setTitle("Input nomor KTP salah");
        alert_ktp.setMessage("Nomor KTP terdiri dari 16 digit angka");

        final AlertDialog.Builder alert_spinner = new AlertDialog.Builder(this);
        alert_spinner.setTitle("Maaf, ada yang kurang");
        alert_spinner.setMessage("Silahkan pilih jenis perizinan anda");

        final AlertDialog.Builder alert_syarat = new AlertDialog.Builder(this);
        alert_syarat.setTitle("Syarat perizinan tidak ditemukan");
        alert_syarat.setMessage("Silahkan pilih jenis perizinan terlebih dahulu");

        final AlertDialog.Builder alert_default = new AlertDialog.Builder(this);
        alert_default.setTitle("Data belum lengkap");
        alert_default.setMessage("Silahkan lengkapi data terlebih dahulu");

        // add a button
        alert_ktp.setPositiveButton("OK", null);
        alert_spinner.setPositiveButton("OK", null);
        alert_syarat.setPositiveButton("OK", null);
        alert_default.setPositiveButton("OK", null);

        editText_nama = (EditText) findViewById(R.id.daftar_nama);
        editText_ktp = (EditText) findViewById(R.id.daftar_no_ktp);

        textView_link_syarat = (TextView) findViewById(R.id.tv_link_syarat_izin);
        textView_link_syarat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinner_jenis_perizinan.getSelectedItemPosition() != 0) {
                    int number = spinner_jenis_perizinan.getSelectedItemPosition();
                    Intent intent = new Intent(DaftarIzinActivity.this, DetailSyaratIzinActivity.class);
                    intent.putExtra("list", number - 1);
                    startActivity(intent);
                } else if (spinner_jenis_perizinan.getSelectedItemPosition() == 0) {
                    AlertDialog dialog = alert_syarat.create();
                    dialog.show();
                }
            }
        });

        spinner_jenis_perizinan = (Spinner) findViewById(R.id.spinner_jenis_perizinan);

        button_daftar = (Button) findViewById(R.id.btn_daftar_izin);
        button_daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                kondisi nama, ktp, pilihan izin, checkbox not null
                if (editText_nama.getText() != null
                        && editText_ktp.getText().length() == 16
                        && spinner_jenis_perizinan.getSelectedItemPosition() != 0
                        && checkBox_persyaratan.isChecked()) {
                    Util.NAMA_DAFTAR_IZIN = editText_nama.getText().toString().trim().toUpperCase();
                    Util.KTP_DAFTAR_IZIN = editText_ktp.getText().toString().trim();
                    int position = spinner_jenis_perizinan.getSelectedItemPosition();
                    Intent intent = new Intent(DaftarIzinActivity.this, UploadBerkasActivity.class);
                    intent.putExtra("position", position - 1);
                    startActivity(intent);
                }
                if (editText_nama.length() == 0 || editText_ktp.length() == 0) {
                    AlertDialog dialog = alert_default.create();
                    dialog.show();
                } else if (editText_nama.length() != 0 && editText_ktp.length() != 16) {
                    AlertDialog dialog = alert_ktp.create();
                    dialog.show();
                } else if (spinner_jenis_perizinan.getSelectedItemPosition() == 0) {
                    AlertDialog dialog = alert_spinner.create();
                    dialog.show();
                }
            }
        });
        textView_link_syarat = (TextView) findViewById(R.id.tv_link_syarat_izin);

        checkBox_persyaratan = (CheckBox) findViewById(R.id.checkbox_persyaratan);
        checkBox_persyaratan.setOnCheckedChangeListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_daftar_izin);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle("Ajukan Perizinan");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final Spinner spinner = (Spinner) findViewById(R.id.spinner_jenis_perizinan);

        String[] perizinan = new String[]{
                "Pilih jenis perizinan...",
                getString(R.string.judul_izin_0),
                getString(R.string.judul_izin_1),
                getString(R.string.judul_izin_2),
                getString(R.string.judul_izin_3),
                getString(R.string.judul_izin_4),
                getString(R.string.judul_izin_5),
                getString(R.string.judul_izin_6),
                getString(R.string.judul_izin_7),
                getString(R.string.judul_izin_8),
                getString(R.string.judul_izin_9),
                getString(R.string.judul_izin_10),
                getString(R.string.judul_izin_11),
                getString(R.string.judul_izin_12),
                getString(R.string.judul_izin_13),
                getString(R.string.judul_izin_14),
                getString(R.string.judul_izin_15),
                getString(R.string.judul_izin_16),
                getString(R.string.judul_izin_17),
                getString(R.string.judul_izin_18),
                getString(R.string.judul_izin_19),
                getString(R.string.judul_izin_20)
        };

        final List<String> list_perizinan = new ArrayList<>(Arrays.asList(perizinan));

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, list_perizinan) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 0) {
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getApplicationContext(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (checkBox_persyaratan.isChecked()) {
            button_daftar.setBackground(getDrawable(R.drawable.button_on));
            button_daftar.setTextColor(getColor(R.color.white));
            button_daftar.setEnabled(true);
        } else {
            button_daftar.setBackground(getDrawable(R.drawable.button_off));
            button_daftar.setTextColor(getColor(R.color.lightGrey));
        }
    }

}
