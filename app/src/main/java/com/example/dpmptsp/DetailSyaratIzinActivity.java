package com.example.dpmptsp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class DetailSyaratIzinActivity extends AppCompatActivity {

    private TextView judul_izin;
    private TextView dasar_izin;
    private TextView waktu_izin;
    private TextView syarat_izin;

    int list = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_syarat_izin);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail_syarat_izin);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle("Persyaratan Perizinan");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        judul_izin = findViewById(R.id.tv_judul);
        dasar_izin = findViewById(R.id.tv_dasar);
        waktu_izin = findViewById(R.id.tv_waktu);
        syarat_izin = findViewById(R.id.tv_syarat);

        try {
            Bundle extras = getIntent().getExtras();
            list = extras.getInt("list");
        } catch (Exception e) {
            e.printStackTrace();
        }

        String[] judul = new String[22];
        String[] dasar = new String[22];
        String[] waktu = new String[22];
        String[] syarat = new String[22];

        judul[0] = getString(R.string.judul_izin_0);
        dasar[0] = getString(R.string.dasar_izin_0);
        waktu[0] = getString(R.string.waktu_izin_0);
        syarat[0] = getString(R.string.syarat_izin_0);

        judul[1] = getString(R.string.judul_izin_1);
        dasar[1] = getString(R.string.dasar_izin_1);
        waktu[1] = getString(R.string.waktu_izin_1);
        syarat[1] = getString(R.string.syarat_izin_1);

        judul[2] = getString(R.string.judul_izin_2);
        dasar[2] = getString(R.string.dasar_izin_2);
        waktu[2] = getString(R.string.waktu_izin_2);
        syarat[2] = getString(R.string.syarat_izin_2);

        judul[3] = getString(R.string.judul_izin_3);
        dasar[3] = getString(R.string.dasar_izin_3);
        waktu[3] = getString(R.string.waktu_izin_3);
        syarat[3] = getString(R.string.syarat_izin_3);

        judul[4] = getString(R.string.judul_izin_4);
        dasar[4] = getString(R.string.dasar_izin_4);
        waktu[4] = getString(R.string.waktu_izin_4);
        syarat[4] = getString(R.string.syarat_izin_4);

        judul[5] = getString(R.string.judul_izin_5);
        dasar[5] = getString(R.string.dasar_izin_5);
        waktu[5] = getString(R.string.waktu_izin_5);
        syarat[5] = getString(R.string.syarat_izin_5);

        judul[6] = getString(R.string.judul_izin_6);
        dasar[6] = getString(R.string.dasar_izin_6);
        waktu[6] = getString(R.string.waktu_izin_6);
        syarat[6] = getString(R.string.syarat_izin_6);

        judul[7] = getString(R.string.judul_izin_7);
        dasar[7] = getString(R.string.dasar_izin_7);
        waktu[7] = getString(R.string.waktu_izin_7);
        syarat[7] = getString(R.string.syarat_izin_7);

        judul[8] = getString(R.string.judul_izin_8);
        dasar[8] = getString(R.string.dasar_izin_8);
        waktu[8] = getString(R.string.waktu_izin_8);
        syarat[8] = getString(R.string.syarat_izin_8);

        judul[9] = getString(R.string.judul_izin_9);
        dasar[9] = getString(R.string.dasar_izin_9);
        waktu[9] = getString(R.string.waktu_izin_9);
        syarat[9] = getString(R.string.syarat_izin_9);

        judul[10] = getString(R.string.judul_izin_10);
        dasar[10] = getString(R.string.dasar_izin_10);
        waktu[10] = getString(R.string.waktu_izin_10);
        syarat[10] = getString(R.string.syarat_izin_10);

        judul[11] = getString(R.string.judul_izin_11);
        dasar[11] = getString(R.string.dasar_izin_11);
        waktu[11] = getString(R.string.waktu_izin_11);
        syarat[11] = getString(R.string.syarat_izin_11);

        judul[12] = getString(R.string.judul_izin_12);
        dasar[12] = getString(R.string.dasar_izin_12);
        waktu[12] = getString(R.string.waktu_izin_12);
        syarat[12] = getString(R.string.syarat_izin_12);

        judul[13] = getString(R.string.judul_izin_13);
        dasar[13] = getString(R.string.dasar_izin_13);
        waktu[13] = getString(R.string.waktu_izin_13);
        syarat[13] = getString(R.string.syarat_izin_13);

        judul[14] = getString(R.string.judul_izin_14);
        dasar[14] = getString(R.string.dasar_izin_14);
        waktu[14] = getString(R.string.waktu_izin_14);
        syarat[14] = getString(R.string.syarat_izin_14);

        judul[15] = getString(R.string.judul_izin_15);
        dasar[15] = getString(R.string.dasar_izin_15);
        waktu[15] = getString(R.string.waktu_izin_15);
        syarat[15] = getString(R.string.syarat_izin_15);

        judul[16] = getString(R.string.judul_izin_16);
        dasar[16] = getString(R.string.dasar_izin_16);
        waktu[16] = getString(R.string.waktu_izin_16);
        syarat[16] = getString(R.string.syarat_izin_16);

        judul[17] = getString(R.string.judul_izin_17);
        dasar[17] = getString(R.string.dasar_izin_17);
        waktu[17] = getString(R.string.waktu_izin_17);
        syarat[17] = getString(R.string.syarat_izin_17);

        judul[18] = getString(R.string.judul_izin_18);
        dasar[18] = getString(R.string.dasar_izin_18);
        waktu[18] = getString(R.string.waktu_izin_18);
        syarat[18] = getString(R.string.syarat_izin_18);

        judul[19] = getString(R.string.judul_izin_19);
        dasar[19] = getString(R.string.dasar_izin_19);
        waktu[19] = getString(R.string.waktu_izin_19);
        syarat[19] = getString(R.string.syarat_izin_19);

        judul[20] = getString(R.string.judul_izin_20);
        dasar[20] = getString(R.string.dasar_izin_20);
        waktu[20] = getString(R.string.waktu_izin_20);
        syarat[20] = getString(R.string.syarat_izin_20);

        addList(judul[list], dasar[list], waktu[list], syarat[list]);
    }

    public void addList(String judul, String dasar, String waktu, String syarat) {
        judul_izin.setText(judul);
        dasar_izin.setText(dasar);
        waktu_izin.setText(waktu);
        syarat_izin.setText(syarat);
    }
}
