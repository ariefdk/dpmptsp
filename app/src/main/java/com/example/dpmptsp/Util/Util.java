package com.example.dpmptsp.Util;

import android.graphics.Bitmap;

public class Util {

    public static String URL = "http://c257a618944a.ngrok.io";
    public static String PATH = URL + "/sipt/public/";

    public static String LOGIN = PATH + "login" ;
    public static String REGISTER = PATH + "register";
    public static String GET_PROSES = PATH + "getproses";
    public static String GET_FLOW = PATH + "getflow";
    public static String GET_PROFIL_IMAGE = PATH + "getprofileimg";
    public static String UPDATE_PROFIL_IMAGE = PATH + "updateprofile";

    public static String ADD_IZIN = PATH + "addizin";

    public static String PRINT = PATH + "dokumen_jadi/";

    public static int ID_PROSES;
    public static String MESSAGE;

    public static String ID;
    public static String NAMA;
    public static String EMAIL;
    public static String TELP;

    public static String NAMA_DAFTAR_IZIN;
    public static String KTP_DAFTAR_IZIN;

    public static Bitmap BITMAP;
}
