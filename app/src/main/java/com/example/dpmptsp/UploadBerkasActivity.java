package com.example.dpmptsp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.dpmptsp.Fragment.IzinATLMFragment;
import com.example.dpmptsp.Fragment.IzinApotekerFragment;
import com.example.dpmptsp.Fragment.IzinBidanFragment;
import com.example.dpmptsp.Fragment.IzinElektromedisFragment;
import com.example.dpmptsp.Fragment.IzinFisioterapisFragment;
import com.example.dpmptsp.Fragment.IzinOkupasiTerapisFragment;
import com.example.dpmptsp.Fragment.IzinOptikFragment;
import com.example.dpmptsp.Fragment.IzinPenyehatTradisionalFragment;
import com.example.dpmptsp.Fragment.IzinPerawatAnastesiFragment;
import com.example.dpmptsp.Fragment.IzinPerawatFragment;
import com.example.dpmptsp.Fragment.IzinPerawatGigiFragment;
import com.example.dpmptsp.Fragment.IzinPuskesmasFragment;
import com.example.dpmptsp.Fragment.IzinROFragment;
import com.example.dpmptsp.Fragment.IzinRadiograferFragment;
import com.example.dpmptsp.Fragment.IzinSanitarianFragment;
import com.example.dpmptsp.Fragment.IzinTenagaGiziFragment;
import com.example.dpmptsp.Fragment.IzinTenagaKefarmasianFragment;
import com.example.dpmptsp.Fragment.IzinTerapisWicaraFragment;
import com.example.dpmptsp.Fragment.IzinTransfusiDarahFragment;
import com.example.dpmptsp.Fragment.IzinTukangGigiFragment;
import com.example.dpmptsp.Fragment.izinPerekamMedisFragment;

public class UploadBerkasActivity extends AppCompatActivity {

    public int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_berkas);

        Intent intent = getIntent();

        position = intent.getIntExtra("position", 0);

        if (position == 0) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinPuskesmasFragment()).commit();
        } else if (position == 1) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinTukangGigiFragment()).commit();
        } else if (position == 2) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinPerawatFragment()).commit();
        } else if (position == 3) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinPerawatGigiFragment()).commit();
        } else if (position == 4) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinBidanFragment()).commit();
        } else if (position == 5) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinATLMFragment()).commit();
        } else if (position == 6) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinSanitarianFragment()).commit();
        } else if (position == 7) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinFisioterapisFragment()).commit();
        } else if (position == 8) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinOkupasiTerapisFragment()).commit();
        } else if (position == 9) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinTenagaGiziFragment()).commit();
        } else if (position == 10) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new izinPerekamMedisFragment()).commit();
        } else if (position == 11) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinRadiograferFragment()).commit();
        } else if (position == 12) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinPerawatAnastesiFragment()).commit();
        } else if (position == 13) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinTransfusiDarahFragment()).commit();
        } else if (position == 14) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinApotekerFragment()).commit();
        } else if (position == 15) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinTenagaKefarmasianFragment()).commit();
        } else if (position == 16) {
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinElektromedisFragment()).commit();
        } else if (position == 17){
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinTerapisWicaraFragment()).commit();
        } else if (position == 18){
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinROFragment()).commit();
        } else if (position == 19){
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinPenyehatTradisionalFragment()).commit();
        } else if (position == 20){
            getSupportFragmentManager().beginTransaction().replace(R.id.upload_fragment_container,
                    new IzinOptikFragment()).commit();
        }

    }

}
