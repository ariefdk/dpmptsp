package com.example.dpmptsp;

import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.dpmptsp.Fragment.HistoryFragment;
import com.example.dpmptsp.Fragment.HomeFragment;
import com.example.dpmptsp.Fragment.ProfileFragment;

public class HomeActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.btm_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container,
                    new HomeFragment()).commit();
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.action_home:
                            bottomNavigationView.getMenu().findItem(R.id.action_history).setEnabled(true);
                            selectedFragment = new HomeFragment();
                            break;
                        case R.id.action_history:
                            bottomNavigationView.getMenu().findItem(R.id.action_history).setEnabled(false);
                            selectedFragment = new HistoryFragment();
                            break;
                        case R.id.action_profile:
                            bottomNavigationView.getMenu().findItem(R.id.action_history).setEnabled(true);
                            selectedFragment = new ProfileFragment();
                            break;
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.home_fragment_container,
                            selectedFragment).commit();
                    return true;
                }
            };

    boolean twice = false;

    @Override
    public void onBackPressed() {
        if (twice == true) {
            finish();
            System.exit(0);
        }

        Toast.makeText(HomeActivity.this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                twice = false;
            }
        }, 3000);
        twice = true;
    }
}
