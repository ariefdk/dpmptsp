package com.example.dpmptsp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.Util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static com.example.dpmptsp.Util.Util.REGISTER;

public class RegistrationActivity extends AppCompatActivity {

    private EditText editText_nama;
    private EditText editText_email;
    private EditText editText_no_hp;
    private EditText editText_password;

    private ProgressDialog progressDialog;

    private Button button_sign_up;
    private TextView textView_sign_in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        editText_nama = (EditText) findViewById(R.id.register_nama);
        editText_email = (EditText) findViewById(R.id.register_email);
        editText_no_hp = (EditText) findViewById(R.id.register_nomor_hp);
        editText_password = (EditText) findViewById(R.id.register_password);

        button_sign_up = (Button) findViewById(R.id.btn_register);
        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((editText_nama.length() != 0 && editText_nama.length() <= 100) &&
                        editText_email.length() >= 10 && editText_email.length() <= 100) &&
                        (editText_no_hp.length() >= 6 && editText_no_hp.length() <= 12) &&
                        editText_password.length() >= 8 && editText_password.length() <= 20) {
                    registrasi();
                } else if (editText_nama.length() == 0) {
                    editText_nama.setError("Nama tidak boleh kosong");
                    editText_nama.requestFocus();
                } else if (editText_email.length() <= 10) {
                    editText_email.setError("Email tidak valid");
                    editText_email.requestFocus();
                } else if (editText_no_hp.length() < 6) {
                    editText_no_hp.setError("No HP tidak valid");
                    editText_no_hp.requestFocus();
                } else if (editText_password.length() < 8) {
                    editText_password.setError("Password 8-20 digit");
                    editText_password.requestFocus();
                }
            }
        });

        textView_sign_in = (TextView) findViewById(R.id.tv_sign_in);
        textView_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

    }

    private void registrasi() {
        showProgressDialog();

        final String nama = editText_nama.getText().toString().trim();
        final String email = editText_email.getText().toString().trim();
        final String no_hp = editText_no_hp.getText().toString().trim();
        final String password = editText_password.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                progressDialog.cancel();
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
                                builder.setTitle("Registrasi Berhasil");
                                builder.setMessage("Silahkan melakukan login Aplikasi");
                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                dialog.show();
//                            } else if (status.equals("registered")) {
//                                progressDialog.cancel();
//                                Toast.makeText(RegistrationActivity.this,
//                                        "Registrasi gagal, Email telah terdaftar",
//                                        Toast.LENGTH_SHORT).show();
                            } else if (status == 0) {
                                progressDialog.cancel();
                                Toast.makeText(RegistrationActivity.this,
                                        "Registrasi gagal",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.cancel();
                            Toast.makeText(RegistrationActivity.this,
                                    "Registrasi gagal",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                Toast.makeText(RegistrationActivity.this,
                        "Maaf, Server sedang sibuk",
                        Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new Hashtable<String, String>();
                params.put("nama", nama);
                params.put("email", email);
                params.put("telp", no_hp);
                params.put("password", password);
                return params;
            }
        };
        {
            RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
            requestQueue.add(stringRequest);
        }
    }


    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(RegistrationActivity.this);
            progressDialog.setMessage("Sign up...");
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
    }

    boolean twice = false;

    @Override
    public void onBackPressed() {
        if (twice == true) {
            finish();
            System.exit(0);
        }

        Toast.makeText(RegistrationActivity.this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                twice = false;
            }
        }, 3000);
        twice = true;
    }
}
