package com.example.dpmptsp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.Model.DataFlow;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import static com.example.dpmptsp.Util.Util.GET_FLOW;
import static com.example.dpmptsp.Util.Util.ID_PROSES;
import static com.example.dpmptsp.Util.Util.MESSAGE;

public class DetailHistoryActivity extends AppCompatActivity {

    private ArrayList<DataFlow> list;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private ProgressBar progressBar;

    private TextView textViewMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail_history);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle("Status Dokumen Perizinan");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        textViewMessage = (TextView) findViewById(R.id.tv_detail_history_message);

        progressBar = (ProgressBar) findViewById(R.id.pb_detail_history);

        list = new ArrayList<>();
        addData(ID_PROSES, MESSAGE);

        recyclerView = (RecyclerView) findViewById(R.id.rv_detail_history);
    }

    public void addData(final int id_proses, final String message) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_FLOW,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("flow");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String tanggal = jsonArray.getJSONObject(i).getString("time");
                                    String flow = jsonArray.getJSONObject(i).getString("flow");
                                    if (!flow.equals("null")){
                                        list.add(new DataFlow(tanggal, flow));
                                    }
                                }
                                textViewMessage.setVisibility(View.GONE);
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                adapter = new rv_adapter(getApplicationContext(), list);
                                recyclerView.setAdapter(adapter);
                                progressBar.setVisibility(View.GONE);
                            } else {
                                textViewMessage.setText(message);
                                textViewMessage.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                            }
                        } catch (
                                JSONException e) {
                            e.printStackTrace();
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(DetailHistoryActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("id_proses", String.valueOf(id_proses));
                return params;
            }
        };

        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }

    private void addList(String tanggal, String flow) {
        list.add(new DataFlow(tanggal, flow));
    }

    class rv_adapter extends RecyclerView.Adapter<rv_adapter.rv_holder> implements View.OnClickListener {

        LayoutInflater inflater;
        ArrayList<DataFlow> flow;

        public rv_adapter(Context context, ArrayList<DataFlow> flow) {
            this.inflater = LayoutInflater.from(context);
            this.flow = flow;
        }

        @NonNull
        @Override
        public rv_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.rv_row_flow, parent, false);
            return new rv_holder(itemView, this);
        }

        @Override
        public void onBindViewHolder(@NonNull rv_holder holder, int position) {
            DataFlow current = flow.get(position);
            holder.tanggal.setText(current.getTanggal());
            holder.flow.setText(current.getFlow());
        }

        @Override
        public int getItemCount() {
            return flow.size();
        }

        @Override
        public void onClick(View v) {
        }

        class rv_holder extends RecyclerView.ViewHolder {

            private RecyclerView.Adapter adapter;
            private TextView tanggal;
            private TextView flow;

            public rv_holder(View itemView, rv_adapter adapter) {
                super(itemView);
                tanggal = (TextView) itemView.findViewById(R.id.tv_flow_tanggal);
                flow = (TextView) itemView.findViewById(R.id.tv_flow_flow);

                this.adapter = adapter;
            }

        }
    }
}
