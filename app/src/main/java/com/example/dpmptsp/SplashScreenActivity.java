package com.example.dpmptsp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.Util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

import static com.example.dpmptsp.Util.Util.LOGIN;

public class SplashScreenActivity extends AppCompatActivity {

    private ImageView logo;
    private static int timeout = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        logo = (ImageView) findViewById(R.id.logo_splash);

        Animation myanim = AnimationUtils.loadAnimation(this, R.anim.splashscreenanimation);
        logo.startAnimation(myanim);

        LoginActivity.sp = getSharedPreferences("login", MODE_PRIVATE);

        if (LoginActivity.sp.getBoolean("logged", false)) {
            login();
        } else {
            gotohome();
        }
    }

    private void login() {
        final String email = LoginActivity.sp.getString("email", "");
        final String password = LoginActivity.sp.getString("pass", "");

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String id = jsonObject.getString("id");
                                String nama = jsonObject.getString("nama");
                                String email = jsonObject.getString("email");
                                String telp = jsonObject.getString("telp");

                                Util.ID = id;
                                Util.NAMA = nama;
                                Util.EMAIL = email;
                                Util.TELP = telp;

                                Intent intent = new Intent(SplashScreenActivity.this, HomeActivity.class);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            gotohome();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                gotohome();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        {
            RequestQueue requestQueue = Volley.newRequestQueue(SplashScreenActivity.this);
            requestQueue.add(stringRequest);
        }
    }

    private void gotohome() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, timeout);
    }

}

