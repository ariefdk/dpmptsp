package com.example.dpmptsp.Model;

public class DataFlow {

    public String tanggal;
    public String flow;

    public DataFlow(String tanggal, String flow) {
        this.tanggal = tanggal;
        this.flow = flow;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }
}
