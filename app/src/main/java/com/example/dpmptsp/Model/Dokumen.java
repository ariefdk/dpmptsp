package com.example.dpmptsp.Model;

public class Dokumen {

    public String nama_dokumen;
    public int id_proses;
    public int position;

    public Dokumen(String nama_dokumen, int id_proses, int position) {
        this.nama_dokumen = nama_dokumen;
        this.id_proses = id_proses;
        this.position = position;
    }

    public String getNama_dokumen() {
        return nama_dokumen;
    }

    public void setNama_dokumen(String nama_dokumen) {
        this.nama_dokumen = nama_dokumen;
    }

    public int getId_proses() {
        return id_proses;
    }

    public void setId_proses(int id_proses) {
        this.id_proses = id_proses;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
