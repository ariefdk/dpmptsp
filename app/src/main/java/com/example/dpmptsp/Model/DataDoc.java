package com.example.dpmptsp.Model;

public class DataDoc {

    public String nama_dokumen;
    public String status_dokumen;
    public String message;
    public int id_dokumen;
    public int position;

    public DataDoc(String nama_dokumen, String status_dokumen, String message, int id_dokumen, int position) {
        this.nama_dokumen = nama_dokumen;
        this.status_dokumen = status_dokumen;
        this.message = message;
        this.id_dokumen = id_dokumen;
        this.position = position;
    }

    public String getNama_dokumen() {
        return nama_dokumen;
    }

    public void setNama_dokumen(String nama_dokumen) {
        this.nama_dokumen = nama_dokumen;
    }

    public String getStatus_dokumen() {
        return status_dokumen;
    }

    public void setStatus_dokumen(String status_dokumen) {
        this.status_dokumen = status_dokumen;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId_dokumen() {
        return id_dokumen;
    }

    public void setId_dokumen(int id_dokumen) {
        this.id_dokumen = id_dokumen;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
