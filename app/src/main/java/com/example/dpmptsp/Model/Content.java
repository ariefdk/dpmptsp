package com.example.dpmptsp.Model;

public class Content {

    public String nama_izin;
    public int position;

    public Content(String nama_izin, int position) {

        this.nama_izin = nama_izin;
        this.position = position;
    }

    public String getNama_izin() {
        return nama_izin;
    }

    public int getPosition() {
        return position;
    }

    public void setNama_izin(String nama_izin) {
        this.nama_izin = nama_izin;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
