package com.example.dpmptsp.Fragment;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.LoginActivity;
import com.example.dpmptsp.R;
import com.example.dpmptsp.Util.Util;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.dpmptsp.Util.Util.BITMAP;
import static com.example.dpmptsp.Util.Util.GET_PROFIL_IMAGE;
import static com.example.dpmptsp.Util.Util.UPDATE_PROFIL_IMAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private ImageButton imageButtonGantiFoto;
    private CircleImageView imageViewFotoProfil;
    private static TextView textViewNama;
    private static TextView textViewEmail;
    private static TextView textViewNoHP;

    private Button buttonLogout;

    public final int SELECT_FILE = 0;

    private RelativeLayout relativeLayoutTransparent;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        imageButtonGantiFoto = view.findViewById(R.id.btn_ganti_foto);
        imageViewFotoProfil = view.findViewById(R.id.iv_foto_profil);
        textViewNama = view.findViewById(R.id.tv_profil_nama);
        textViewEmail = view.findViewById(R.id.tv_profil_email);
        textViewNoHP = view.findViewById(R.id.tv_profil_no_hp);

        buttonLogout = view.findViewById(R.id.btn_logout);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                logout();
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Apakah Anda yakin akan logout?").setPositiveButton("Ya", dialogClickListener)
                        .setNegativeButton("Tidak", dialogClickListener).show();
            }
        });

        relativeLayoutTransparent = view.findViewById(R.id.profil_transparent);

        textViewEmail.setText(Util.EMAIL);
        textViewNama.setText(Util.NAMA);
        textViewNoHP.setText(Util.TELP);
        imageViewFotoProfil.setImageBitmap(Util.BITMAP);

        Bitmap bitmapDrawable = ((BitmapDrawable) imageViewFotoProfil.getDrawable()).getBitmap();

        if (bitmapDrawable == null) {
            getProfilImage();
        }

        imageButtonGantiFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImage();
            }
        });

        imageViewFotoProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectImage();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    private void logout() {
        SharedPreferences.Editor editor = LoginActivity.sp.edit();
        editor.clear();
        editor.apply();
        Intent i = new Intent(getActivity(), LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    private void getProfilImage() {
        relativeLayoutTransparent.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_PROFIL_IMAGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String pic = jsonObject.getString("pic");
                            if (pic != null) {
                                byte[] decodedString = Base64.decode(pic, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                BITMAP = decodedByte;
                                imageViewFotoProfil.setImageBitmap(decodedByte);
                            } else {
                                imageViewFotoProfil.setBackgroundResource(R.drawable.account);
                                relativeLayoutTransparent.setVisibility(View.GONE);
                            }
                        } catch (
                                JSONException e) {
                            e.printStackTrace();
                        }
                        relativeLayoutTransparent.setVisibility(View.GONE);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        relativeLayoutTransparent.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_LONG).show();
                        relativeLayoutTransparent.setVisibility(View.GONE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new Hashtable<String, String>();

                params.put("email", Util.EMAIL);
                return params;
            }
        };
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }
    }

    private void SelectImage() {

        final CharSequence[] items = {"Galeri", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Ubah foto profil");

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Galeri")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    //startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE);
                    startActivityForResult(intent, SELECT_FILE);

                } else if (items[i].equals("Batal")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Uri filepath = data.getData();
            if (requestCode == SELECT_FILE) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filepath);
                    Bitmap upload = Bitmap.createScaledBitmap(bitmap, 600, 600, false);

                    try {
                        InputStream in = getActivity().getContentResolver().openInputStream(filepath);
                        Bitmap fix = rotateImage(upload, in);
                        Util.BITMAP = fix;
                        imageViewFotoProfil.setImageBitmap(fix);
                        //encoding image to string
                        String image = getStringImage(fix);
                        Log.d("image", image);
                        //passing the image to volley
                        uploadImage(image);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Bitmap rotateImage(Bitmap bitmap, InputStream filepath) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(filepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            default:
                break;
        }
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void uploadImage(final String image) {
        relativeLayoutTransparent.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATE_PROFIL_IMAGE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                relativeLayoutTransparent.setVisibility(View.GONE);
                                Toast.makeText(getContext(),
                                        "Foto profil berhasil diubah!",
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                relativeLayoutTransparent.setVisibility(View.GONE);
                                Toast.makeText(getContext(),
                                        "Error!",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (
                                JSONException e) {
                            relativeLayoutTransparent.setVisibility(View.GONE);
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_LONG).show();
                        relativeLayoutTransparent.setVisibility(View.GONE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email", Util.EMAIL);
                params.put("image", image);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }
    }
}

