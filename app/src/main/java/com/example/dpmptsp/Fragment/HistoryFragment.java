package com.example.dpmptsp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.DetailHistoryActivity;
import com.example.dpmptsp.DetailSyaratIzinActivity;
import com.example.dpmptsp.HomeActivity;
import com.example.dpmptsp.Model.DataDoc;
import com.example.dpmptsp.R;
import com.example.dpmptsp.Util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import static com.example.dpmptsp.Util.Util.GET_PROSES;
import static com.example.dpmptsp.Util.Util.ID_PROSES;
import static com.example.dpmptsp.Util.Util.MESSAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {

    private ArrayList<DataDoc> list;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private TextView textViewBelumAdaRiwayat;
    private ProgressBar progressBar;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_history, container, false);

        textViewBelumAdaRiwayat = view.findViewById(R.id.tv_blm_ada_riwayat);
        progressBar = view.findViewById(R.id.pb_riwayat);

        addData();

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_riwayat);

        // Inflate the layout for this fragment
        return view;
    }

    public void addData() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_PROSES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            list = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("proses");
                            if (jsonArray.length() != 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    String namaIzin = jsonArray.getJSONObject(i).getString("nama_izin");
                                    String statusIzin = jsonArray.getJSONObject(i).getString("status");
                                    String namaPemohon = jsonArray.getJSONObject(i).getString("nama_pemohon");
                                    int idProses = jsonArray.getJSONObject(i).getInt("id_proses");
                                    String message = jsonArray.getJSONObject(i).getString("message");
                                    list.add(new DataDoc(namaIzin + "\nPemohon: " + namaPemohon , statusIzin, message, idProses, i));
                                }
                                recyclerView.setNestedScrollingEnabled(false);
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext()));
                                adapter = new rv_adapter(getContext(), list);
                                recyclerView.setAdapter(adapter);
                                progressBar.setVisibility(View.GONE);
                            } else {
                                progressBar.setVisibility(View.GONE);
                                textViewBelumAdaRiwayat.setVisibility(View.VISIBLE);
                            }
                        } catch (
                                JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), "Catch", Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("pemohon", Util.ID);
                return params;
            }
        };

        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }
    }

    private void addList(String nama_izin, String status, String message, int id, int position) {
        list.add(new DataDoc(nama_izin, status, message, id, position));
    }

    class rv_adapter extends RecyclerView.Adapter<rv_adapter.rv_holder> implements View.OnClickListener {

        LayoutInflater inflater;
        ArrayList<DataDoc> dataDokumen;

        public rv_adapter(Context context, ArrayList<DataDoc> dataDokumen) {
            this.inflater = LayoutInflater.from(context);
            this.dataDokumen = dataDokumen;
        }

        @NonNull
        @Override
        public rv_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.rv_row_riwayat, parent, false);
            return new rv_holder(itemView, this);
        }

        @Override
        public void onBindViewHolder(@NonNull rv_holder holder, int position) {
            DataDoc current = dataDokumen.get(position);
            holder.namaDokumen.setText(current.getNama_dokumen());
            if (current.getMessage().equals("")) {
                if (current.getStatus_dokumen().equals("belum diproses")) {
                    holder.statusDokumen.setText("Menunggu validasi oleh Front Office");
                    holder.cardViewStatus.setCardBackgroundColor(getResources().getColor(R.color.lightGrey));
                } else if (current.getStatus_dokumen().equals("sedang diproses")) {
                    holder.statusDokumen.setText("Berkas sedang diproses");
                    holder.cardViewStatus.setCardBackgroundColor(getResources().getColor(R.color.orange));
                } else if (current.getStatus_dokumen().equals("selesai")) {
                    holder.statusDokumen.setText("Dokumen perizinan siap diambil di Kantor Dinas Perizinan");
                    holder.cardViewStatus.setCardBackgroundColor(getResources().getColor(R.color.green));
                }
            } else {
                holder.statusDokumen.setText("Berkas persyaratan tidak valid");
                holder.cardViewStatus.setBackgroundColor(getResources().getColor(R.color.red));
            }
            holder.message = current.getMessage();
            holder.position = current.getPosition();
            holder.id = current.getId_dokumen();
        }

        @Override
        public int getItemCount() {
            return dataDokumen.size();
        }

        @Override
        public void onClick(View v) {
        }

        class rv_holder extends RecyclerView.ViewHolder {

            private RecyclerView.Adapter adapter;
            private TextView namaDokumen;
            private TextView statusDokumen;
            private RelativeLayout relativeLayout;
            private CardView cardViewStatus;
            private Button buttonCek;
            private String message;
            private int id;
            private int position;

            public rv_holder(View itemView, rv_adapter adapter) {
                super(itemView);
                namaDokumen = (TextView) itemView.findViewById(R.id.tv_nama_dokumen);
                statusDokumen = (TextView) itemView.findViewById(R.id.tv_status_dokumen);
                relativeLayout = (RelativeLayout) itemView.findViewById(R.id.layout_riwayat);
                cardViewStatus = (CardView) itemView.findViewById(R.id.cv_status_dokumen);

                buttonCek = (Button) itemView.findViewById(R.id.btn_cek);

                this.adapter = adapter;

                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v == relativeLayout) {
                            Intent intent = new Intent(getActivity(), DetailHistoryActivity.class);
                            ID_PROSES = id;
                            MESSAGE = message;
                            startActivity(intent);
                        }
                    }
                });

                buttonCek.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), DetailHistoryActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("id_proses", list.get(position).getId_dokumen());
                        bundle.putString("message", list.get(position).getMessage());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }

        }

    }
}
