package com.example.dpmptsp.Fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.HomeActivity;
import com.example.dpmptsp.R;
import com.example.dpmptsp.Util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;

import static android.Manifest.permission.CAMERA;
import static com.example.dpmptsp.Util.Util.ADD_IZIN;

/**
 * A simple {@link Fragment} subclass.
 */
public class IzinPenyehatTradisionalFragment extends Fragment implements View.OnClickListener{

    private ImageView imageView1, imageView2, imageView3, imageView4, imageView5, imageView6;
    private Button button1, button2, button3, button4, button5, button6;

    public String imageData1, imageData2, imageData3, imageData4, imageData5, imageData6;
    String[] imageDatas = {imageData1, imageData2, imageData3, imageData4, imageData5, imageData6};

    private Button buttonUpload;

    public final int REQUEST_CAMERA = 1;
    public final int SELECT_FILE = 0;

    private int position = 0;

    private ProgressDialog progressDialog;

    public IzinPenyehatTradisionalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_izin_penyehat_tradisional, container, false);

        imageView1 = view.findViewById(R.id.pic1_penyehat_tradisional);
        imageView2 = view.findViewById(R.id.pic2_penyehat_tradisional);
        imageView3 = view.findViewById(R.id.pic3_penyehat_tradisional);
        imageView4 = view.findViewById(R.id.pic4_penyehat_tradisional);
        imageView5 = view.findViewById(R.id.pic5_penyehat_tradisional);
        imageView6 = view.findViewById(R.id.pic6_penyehat_tradisional);

        button1 = view.findViewById(R.id.btn_add1_penyehat_tradisional);
        button1.setOnClickListener(this);
        button2 = view.findViewById(R.id.btn_add2_penyehat_tradisional);
        button2.setOnClickListener(this);
        button3 = view.findViewById(R.id.btn_add3_penyehat_tradisional);
        button3.setOnClickListener(this);
        button4 = view.findViewById(R.id.btn_add4_penyehat_tradisional);
        button4.setOnClickListener(this);
        button5 = view.findViewById(R.id.btn_add5_penyehat_tradisional);
        button5.setOnClickListener(this);
        button6 = view.findViewById(R.id.btn_add6_penyehat_tradisional);
        button6.setOnClickListener(this);

        buttonUpload = view.findViewById(R.id.btn_upload_berkas_penyehat_tradisional);
        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();
            }
        });

        int currentApiVersion = Build.VERSION.SDK_INT;

        if (currentApiVersion >= Build.VERSION_CODES.M) {
            if (checkPermission()) {

            } else {
                requestPermission();
            }
        }

        // Inflate the layout for this fragment
        return view;
    }

    private void checkData() {
        if (imageDatas[0] != null && imageDatas[1] != null && imageDatas[2] != null &&
                imageDatas[3] != null && imageDatas[4] != null && imageDatas[5] != null) {
            buttonUpload.setBackground(getActivity().getDrawable(R.drawable.button_on));
            buttonUpload.setTextColor(getActivity().getColor(R.color.white));
            buttonUpload.setEnabled(true);
        }
    }

    private boolean checkPermission() {
        return (ContextCompat.checkSelfPermission(getActivity(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{CAMERA}, REQUEST_CAMERA);
    }

    private void selectImage() {

        final CharSequence[] items = {"Kamera", "Galeri", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Upload berkas");

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Kamera")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[i].equals("Galeri")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    //startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE);
                    startActivityForResult(intent, SELECT_FILE);

                } else if (items[i].equals("Cancel")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImageView[] imageViews = {imageView1, imageView2, imageView3, imageView4, imageView5, imageView6};

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bundle bundle = data.getExtras();
                final Bitmap bitmap = (Bitmap) bundle.get("data");
                Bitmap upload = Bitmap.createScaledBitmap(bitmap, 600, 600, false);
                imageViews[position].setImageBitmap(upload);
                imageViews[position].setVisibility(View.VISIBLE);
                //encoding image to string
                this.imageDatas[position] = getStringImage(upload);
            } else if (requestCode == SELECT_FILE) {
                Uri filepath = data.getData();
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), filepath);
                    Bitmap upload = Bitmap.createScaledBitmap(bitmap, 600, 600, false);
                    try {
                        InputStream in = getActivity().getContentResolver().openInputStream(filepath);
                        Bitmap fix = rotateImage(upload, in);
                        imageViews[position].setImageBitmap(fix);
                        imageViews[position].setVisibility(View.VISIBLE);
                        //encoding image to string
                        this.imageDatas[position] = getStringImage(fix);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            checkData();
        }
    }

    private Bitmap rotateImage(Bitmap bitmap, InputStream filepath) {
        ExifInterface exifInterface = null;
        try {
            exifInterface = new ExifInterface(filepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(270);
                break;
            default:
                break;
        }
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        return rotatedBitmap;
    }

    public String getStringImage(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        return encodedImage;
    }

    private void uploadImage() {
        showProgressDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ADD_IZIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String status = jsonObject.getString("status");
                            if (status.equals("1")) {
                                progressDialog.cancel();
                                Toast.makeText(getContext(),
                                        "Berkas berhasil diupload",
                                        Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            } else {
                                progressDialog.cancel();
                                Toast.makeText(getContext(),
                                        "Error!",
                                        Toast.LENGTH_SHORT).show();
                            }

                        } catch (
                                JSONException e) {
                            progressDialog.cancel();
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.cancel();
                        Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new Hashtable<String, String>();
                params.put("kode_izin", "IPT");
                params.put("id", Util.ID);
                params.put("email", Util.EMAIL);
                params.put("ktp", Util.KTP_DAFTAR_IZIN);
                params.put("nama", Util.NAMA_DAFTAR_IZIN);
                params.put("image1", imageDatas[0]);
                params.put("image2", imageDatas[1]);
                params.put("image3", imageDatas[2]);
                params.put("image4", imageDatas[3]);
                params.put("image5", imageDatas[4]);
                params.put("image6", imageDatas[5]);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        {
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add1_penyehat_tradisional:
                position = 0;
                selectImage();
                break;
            case R.id.btn_add2_penyehat_tradisional:
                position = 1;
                selectImage();
                break;
            case R.id.btn_add3_penyehat_tradisional:
                position = 2;
                selectImage();
                break;
            case R.id.btn_add4_penyehat_tradisional:
                position = 3;
                selectImage();
                break;
            case R.id.btn_add5_penyehat_tradisional:
                position = 4;
                selectImage();
                break;
            case R.id.btn_add6_penyehat_tradisional:
                position = 5;
                selectImage();
                break;
            default:
                break;
        }
    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Mengupload berkas...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
    }

}
