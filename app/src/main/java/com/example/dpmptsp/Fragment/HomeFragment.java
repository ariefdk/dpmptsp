package com.example.dpmptsp.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.dpmptsp.DaftarIzinActivity;
import com.example.dpmptsp.DetailSyaratIzinActivity;
import com.example.dpmptsp.DokumenActivity;
import com.example.dpmptsp.LoginActivity;
import com.example.dpmptsp.Model.Content;
import com.example.dpmptsp.R;
import com.example.dpmptsp.Util.Util;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ArrayList<Content> list;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private Button buttonAjukan;
    private Button buttonDokumen;

    private TextView textViewNama;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        textViewNama = (TextView) view.findViewById(R.id.tv_home_nama);

        textViewNama.setText("Halo, " + Util.NAMA);

        buttonDokumen = (Button) view.findViewById(R.id.btn_dokumen_saya);
        buttonAjukan = (Button) view.findViewById(R.id.btn_ajukan_perizinan);

        buttonDokumen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DokumenActivity.class);
                startActivity(intent);
            }
        });

        buttonAjukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DaftarIzinActivity.class);
                startActivity(intent);
            }
        });

        addList();

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_info);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        adapter = new rv_adapter(getContext(), list);
        recyclerView.setAdapter(adapter);

//         Inflate the layout for this fragment
        return view;
    }

    private void addList() {
        list = new ArrayList<>();
        list.add(new Content("Izin Operasional Penyelenggaraan Puskesmas", 0));
        list.add(new Content("Izin Tukang Gigi", 1));
        list.add(new Content("Izin Praktik Perawat", 2));
        list.add(new Content("Izin Praktik Perawat Gigi", 3));
        list.add(new Content("Izin Praktik Bidan", 4));
        list.add(new Content("Izin Praktik Analis/ATLM", 5));
        list.add(new Content("Izin Praktik Sanitarian", 6));
        list.add(new Content("Izin Praktik Fisioterapis", 7));
        list.add(new Content("Izin Praktik Okupasi Terapis", 8));
        list.add(new Content("Izin Praktik Tenaga Gizi", 9));
        list.add(new Content("Izin Praktik Perekam Medis", 10));
        list.add(new Content("Izin Praktik Radiografer", 11));
        list.add(new Content("Izin Praktik Perawat Anastesi", 12));
        list.add(new Content("Izin Praktik Teknisi Transfusi Darah", 13));
        list.add(new Content("Izin Praktik Apoteker", 14));
        list.add(new Content("Izin Praktik Tenaga Teknis Kefarmasian", 15));
        list.add(new Content("Izin Praktik Elektromedis", 16));
        list.add(new Content("Izin Praktik Terapis Wicara", 17));
        list.add(new Content("Izin Praktik Refraksionis Optisien (RO)", 18));
        list.add(new Content("Izin Penyehat Tradisional", 19));
        list.add(new Content("Izin Penyelenggaraan Optik", 20));
    }

    class rv_adapter extends RecyclerView.Adapter<rv_adapter.rv_holder> implements View.OnClickListener {

        LayoutInflater inflater;
        ArrayList<Content> dataNama;

        public rv_adapter(Context context, ArrayList<Content> dataNama) {
            this.inflater = LayoutInflater.from(context);
            this.dataNama = dataNama;
        }

        @NonNull
        @Override
        public rv_holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = inflater.inflate(R.layout.rv_row_info, parent, false);
            return new rv_holder(itemView, this);
        }

        @Override
        public void onBindViewHolder(@NonNull rv_holder holder, int position) {
            Content current = dataNama.get(position);
            holder.namaIzin.setText(current.getNama_izin());
            holder.position = current.getPosition();
        }

        @Override
        public int getItemCount() {
            return dataNama.size();
        }

        @Override
        public void onClick(View v) {
        }

        class rv_holder extends RecyclerView.ViewHolder {

            private RecyclerView.Adapter adapter;
            private TextView namaIzin;
            private RelativeLayout relativeLayout;

            private Button buttonGo;

            int position;

            public rv_holder(View itemView, rv_adapter adapter) {
                super(itemView);
                namaIzin = (TextView) itemView.findViewById(R.id.tv_nama_izin);
                relativeLayout = (RelativeLayout) itemView.findViewById(R.id.layout_info);

                buttonGo = (Button) itemView.findViewById(R.id.btn_go);

                this.adapter = adapter;

                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (v == relativeLayout) {
                            Intent intent = new Intent(getActivity(), DetailSyaratIzinActivity.class);
                            intent.putExtra("list", position);
                            startActivity(intent);
                        }
                    }
                });

                buttonGo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), DetailSyaratIzinActivity.class);
                        intent.putExtra("list", position);
                        startActivity(intent);
                    }
                });
            }
        }
    }
}