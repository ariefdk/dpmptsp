package com.example.dpmptsp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.dpmptsp.Util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static com.example.dpmptsp.Util.Util.LOGIN;

public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private TextView textViewSignUp;
    public static EditText etLoginEmail;
    public static EditText etLoginPassword;

    private ProgressDialog progressDialog;

    public static SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etLoginEmail = (EditText) findViewById(R.id.login_email);
        etLoginPassword = (EditText) findViewById(R.id.login_password);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((etLoginEmail.length() >= 10 && etLoginEmail.length() <= 100) &&
                        etLoginPassword.length() >= 8 && etLoginPassword.length() <= 20) {
                    login();
                } else {
                    Toast.makeText(LoginActivity.this,
                            "Email/Password tidak valid",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        textViewSignUp = (TextView) findViewById(R.id.tv_sign_up);
        textViewSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });

    }

    private void login() {
        showProgressDialog();

        final String email = etLoginEmail.getText().toString().trim();
        final String password = etLoginPassword.getText().toString().trim();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            if (status == 1) {
                                String id = jsonObject.getString("id");
                                String nama = jsonObject.getString("nama");
                                String email = jsonObject.getString("email");
                                String telp = jsonObject.getString("telp");

                                Util.ID = id;
                                Util.NAMA = nama;
                                Util.EMAIL = email;
                                Util.TELP = telp;

                                sp.edit().putString("email", email).apply();
                                sp.edit().putString("pass", password).apply();
                                sp.edit().putBoolean("logged", true).apply();

                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                            } else if (status == 0) {
                                progressDialog.cancel();
                                Toast.makeText(LoginActivity.this,
                                        "Login gagal, Email/Password salah",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.cancel();
//                            Toast.makeText(LoginActivity.this,
//                                    "Maaf, Server sedang sibuk",
//                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.cancel();
                Toast.makeText(LoginActivity.this,
                        "Maaf, Server sedang sibuk",
                        Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new Hashtable<String, String>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        {
            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            requestQueue.add(stringRequest);
        }
    }

    public void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Log in...");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.show();
    }

    boolean twice = false;

    @Override
    public void onBackPressed() {
        if (twice == true) {
            finish();
            System.exit(0);
        }

        Toast.makeText(LoginActivity.this, "Press BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                twice = false;
            }
        }, 3000);
        twice = true;
    }
}

